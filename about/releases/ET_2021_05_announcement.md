% vim: tw=0
# Release Announcement

We are pleased to announce the twenty-second release (code name "Lorentz") of the Einstein Toolkit, an open-source, community developed software infrastructure for relativistic astrophysics. The highlights of this release include:

* POWER, a Python package to post-process the data products of simulations to compute the gravitational wave strain at future null infinity.
* Simfactory is now fully compatible with Python 2 or 3.

In addition, bug fixes accumulated since the previous release in November 2020 have been included.

The Einstein Toolkit is a collection of software components and tools for simulating and analyzing general relativistic astrophysical systems that builds on numerous software efforts in the numerical relativity community including code to compute initial data parameters, the spacetime evolution codes Baikal, lean_public, and McLachlan, analysis codes to compute horizon characteristics and gravitational waves, the Carpet AMR infrastructure, and the relativistic magneto-hydrodynamics codes GRHydro and IllinoisGRMHD. The Einstein Toolkit also contains a 1D self-force code. For parts of the toolkit, the Cactus Framework is used as the underlying computational infrastructure providing large-scale parallelization, general computational components, and a model for collaborative, portable code development.

The Einstein Toolkit uses a distributed software model and its different modules are developed, distributed, and supported either by the core team of Einstein Toolkit Maintainers, or by individual groups. Where modules are provided by external groups, the Einstein Toolkit Maintainers provide quality control for modules for inclusion in the toolkit and help coordinate support. The Einstein Toolkit Maintainers currently involve staff and faculty from five different institutions, and host weekly meetings that are open for anyone to join.

Guiding principles for the design and implementation of the toolkit include: open, community-driven software development; well thought-out and stable interfaces; separation of physics software from computational science infrastructure; provision of complete working production code; training and education for a new generation of researchers.

For more information about using or contributing to the Einstein Toolkit, or to join the Einstein Toolkit Consortium, please visit our web pages at http://einsteintoolkit.org, or contact the users mailing list users@einsteintoolkit.org.

The Einstein Toolkit is primarily supported by NSF 2004157/2004044/2004311/2004879/2003893 (Enabling fundamental research in the era of multi-messenger astrophysics).

The Einstein Toolkit contains about 327 regression test cases. On a large portion of the tested machines, almost all of these tests pass, using both MPI and OpenMP parallelization.

The changes between this and the previous release include:

## Larger changes since last release

* Vectors: Provide arithmetic operations that mix vector and scalar arguments
* TestLoop: Use OpenMP and OpenMP macros
* GetComponents: Add --no-update option
* CarpetIOHDF5: Improve assertions infrastructure
* TestLoopControl: Add OpenMP code to test
* simfactory2: Many machine configurations updated and modernized
* VolumeIntegrals_vacuum: Add ADM (mass/momentum/angular momentum) integrands
* VolumeIntegrals_vacuum & VolumeIntegrals_GRMHD: Add documentation
* ID_converter_ILGRMHD: Fix option that perturbs initial data
* Llama: Re-allow interpatch boundary interpolation for all Thornburg04
* Cactus: Add CUDA support, fix various compilation issues on modern compilers, fill in missing documentation
* EinsteinAnalysis and CactusBase thorns: Improve and fix documentation

## Contributors

This release includes contributions by Gabriele Bozzola, Steven R. Brandt, Brockton Brendal, Zachariah Etienne, Roland Haas, E. A. Huerta, Daniel Johnson, David Radice, Erik Schnetter, and Leonardo Werneck.

## How to upgrade from DeWitt-Morette (ET_2020_11)

To upgrade from the previous release, use GetComponents with the new thornlist to check out the new version.

See the Download page (http://einsteintoolkit.org/download.html) on the Einstein Toolkit website for download instructions.

The SelfForce-1D code uses a single git repository, thus using 

  git pull; git checkout ET_2021_05

will update the code.

Machine notes

Supported (tested) machines include:

* Default Debian, Ubuntu, Fedora, CentOS 7, Mint, OpenSUSE and MacOS Catalina (MacPorts) installations
* Bluewaters
* Comet
* Cori
* Graham
* Frontera
* Mike / Shelob
* Queen Bee 2
* Queen Bee 3
* Stampede 2
* SuperMUC-NG
* Summit
* Wheeler

Note for individual machines:

* TACC machines: defs.local.ini needs to have `sourcebasedir = $WORK` and `basedir = $SCRATCH/simulations` configured for this machine. You need to determine $WORK and $SCRATCH by logging in to the machine.
* SuperMUC-NG: defs.local.ini needs to have `sourcebasedir = $HOME` and `basedir = $SCRATCH/simulations` configured for this machine. You need to determine $HOME and $SCRATCH by logging in to the machine.

All repositories participating in this release carry a branch ET_2021_05 marking this release. These release branches will be updated if severe errors are found.

The "Lorentz" Release Team on behalf of the Einstein Toolkit Consortium (2021-05-31)

* Zachariah B. Etienne
* Roland Haas
* Steven R. Brandt
* William E. Gabella
* Peter Diener
* Atul Kedia
* Miguel Gracia

May, 2021
