#!/usr/bin/python3
import os, requests, pprint, json, re, sys
import datetime
import argparse

parser = argparse.ArgumentParser(description='Zenodo Tool')
parser.add_argument('--list', action='store_true', default=False, help='List all depositions and exit')
parser.add_argument('--sandbox', action='store_true', default=False, help='Act on Zenodo\'s sandbox server instead of real one')
parser.add_argument('--upload', action='store_true', default=False, help='Upload changes')
parser.add_argument('--create', action='store', default=None, help='Create a new deposit using metadata given by the file argument')
parser.add_argument('--newversion', action='store_true', default=None, help='Create a new version for the deposit')
parser.add_argument('--deposit', action='store', default=None, help='Deposit a file given by the file argument')
parser.add_argument('--publish', action='store_true', default=False, help='Publish changes (cannot be reverted)')
parser.add_argument('--id', action='store', default=None, help='The deposit id to act on')
parser.add_argument('--generate', action='store_true', default=False, help='Whether to create zupload.py and Definition.md')
pres=parser.parse_args(sys.argv[1:])

if "ZENODO_ACCESS" not in os.environ:
    print("You need a zenodo access token to use this tool.")
    sys.exit(1)

access_token = os.environ["ZENODO_ACCESS"]

listdeps = pres.list
sandbox = pres.sandbox
upload = pres.upload
create = pres.create
newversion = pres.newversion
deposit = pres.deposit
publish = pres.publish
generate = pres.generate
id = pres.id

if upload and not id:
    print("You need to provide an id to upload to.")
    sys.exit(1)

if create and not os.access(create, os.R_OK):
    print("'%s' does not exist or is not readable." % create)
    sys.exit(1)

if create and id != None:
    print("You must not provide an id when creating a new deposit")
    sys.exit(1)

if newversion and not id:
    print("You need to provide an id to create a new version for.")
    sys.exit(1)

if create and newversion:
   # TODO: allow this by checking for id being set only if not create
   print("Only one of --create or --newversion is allowed")
   sys.exit(1)

if deposit and not id:
    print("You need to provide an id to deposit to.")
    sys.exit(1)

if deposit and not os.access(deposit, os.R_OK):
    print("'%s' does not exist or is not readable." % deposit)
    sys.exit(1)

if publish and not id:
    print("You need to provide an id to publish.")
    sys.exit(1)

if sandbox:
    server = "sandbox.zenodo.org"
else:
    server = "zenodo.org"

if listdeps:
  deps = requests.get("https://{server}/api/deposit/depositions".format(server=server),params={"access_token":access_token})
  if not deps.ok:
    print("request failed: %s\n%s" % (deps.status_code, deps.json()))
    deps.raise_for_status()
  for dep in deps.json():
    try:
      latest_draft = dep["links"]["latest_draft"]
      m = re.search(r'/([0-9]*)$',latest_draft)
      id = m.group(1)
    except KeyError:
      # no draft, everything submitted
      id = dep["id"]
    print("id:",id,dep['title'])
  exit(0)

if create:
  c = eval(open(create).read())
  c["submitted"] = False
  # doi entries tend to confused Zenodo / interfere with it allocation a new one / cause internal server errors
  # while we are at it, get rid of a couple other entries that make no sense when creating a new deposit
  for entry in ["doi", "doi_url", "files", "id", "links", "metadata.doi", "metadata.prereserve_doi"]:
    final = entry.split(".")[-1]
    sub_c = c
    for pathpart in entry.split(".")[:-1]:
        sub_c = c[pathpart]
    del sub_c[final]
  # request a new DOI
  c["metadata"]["prereserve_doi"] = True

  dep = requests.post("https://{server}/api/deposit/depositions".format(server=server),
       data=json.dumps(c),
       headers={"Content-Type": "application/json"},
       params={"access_token":access_token})
  if not dep.ok:
    print("request failed: %s\n%s" % (dep.status_code, dep.json()))
    dep.raise_for_status()
  c = dep.json()
  id = c["id"]
else:
  assert id is not None, "You need to provide a zenodo id. Try running 'zenodo.py --list'"
  dep = requests.get("https://{server}/api/deposit/depositions/{id}".format(server=server,id=id),params={"access_token":access_token})
  if not dep.ok:
    print("request failed: %s\n%s" % (dep.status_code, dep.json()))
    dep.raise_for_status()
  c = dep.json()

if newversion:
  dep = requests.post("https://{server}/api/deposit/depositions/{id}/actions/newversion".format(server=server,id=id),
       params={"access_token":access_token})
  if not dep.ok:
    print("request faild: %s\n%s" % (dep.status_code, dep.json()))
    dep.raise_for_status()
  c = dep.json()
  # the docs (https://developers.zenodo.org/#new-version) say:
  # > The response body of this action is NOT the new version deposit, but the
  # > original resource. The new version deposition can be accessed through the
  # > "latest_draft" under "links" in the response body. - The id used to create
  # > this new version has to be the id of the latest version. It is not possible
  # > to use the global id that references all the versions.
  # so get that id and data
  dep = requests.get(c["links"]["latest_draft"],params={"access_token":access_token})
  if not dep.ok:
    print("request faild: %s\n%s" % (dep.status_code, dep.json()))
    dep.raise_for_status()
  c = dep.json()
  id = c["id"]

if deposit:
  bucket_url = c["links"]["bucket"]
  with open(deposit, "rb") as fh:
    dep = requests.put("%s/%s" % (bucket_url, os.path.basename(deposit)),
         data=fh,
         # No headers included in the request, since it's a raw byte request
         params={"access_token":access_token})
    if not dep.ok:
      print("request faild: %s\n%s" % (dep.status_code, dep.json()))
      dep.raise_for_status()

creators = {}
with open("developers.txt", "r") as fd:
    for line in fd.readlines():
        cols = line.strip().split(":")
        if cols[0] == "Name":
            continue
        creators[cols[0]] = cols

names = creators.keys()

#===========================================

release_team = [
    "Zachariah Etienne",
    "Roland Haas",
    "Steven R. Brandt",
    "William E. Gabella",
    "Peter Diener",
    "Atul Kedia",
    "Miguel Gracia-Linares"
]

et_release = "ET_2021_05"
et_release_codename = "Lorentz"

definition_md = f"""
The Einstein Toolkit
====================

This DOI refers to {et_release} releases of the codes that comprise the Einstein Toolkit:

 * The Cactus code, which is checked out using ["einsteintoolkit.th"](https://bitbucket.org/einsteintoolkit/manifest/src/{et_release}_v0/einsteintoolkit.th)

 * The SelfForce-1D code (which is checked out using `git clone --single-branch --branch {et_release} https://bitbucket.org/peterdiener/selfforce-1d.git`)

See https://einsteintoolkit.org/download.html for more detailed instructions on downloading and installing the software.
"""

et_description = '<p>The Einstein Toolkit is a&nbsp;<a ' \
 'href="https://einsteintoolkit.org/members.html">community</a>-driven ' \
 'software platform of core computational tools to ' \
 'advance and support research in relativistic ' \
 'astrophysics and gravitational physics.</p>\n' \
 '\n' \
 '<p>The Einstein Toolkit has been supported by ' \
 'NSF&nbsp;<a ' \
 'href="https://nsf.gov/awardsearch/showAward?AWD_ID=2004157&amp;HistoricalAwards=false">2004157</a>/<a ' \
 'href="https://nsf.gov/awardsearch/showAward?AWD_ID=2004044&amp;HistoricalAwards=false">2004044</a>/<a ' \
 'href="https://nsf.gov/awardsearch/showAward?AWD_ID=2004311&amp;HistoricalAwards=false">2004311</a>/<a ' \
 'href="https://nsf.gov/awardsearch/showAward?AWD_ID=2004879&amp;HistoricalAwards=false">2004879</a>/<a ' \
 'href="https://nsf.gov/awardsearch/showAward?AWD_ID=2003893&amp;HistoricalAwards=false">2003893</a>/<a ' \
 'href="https://nsf.gov/awardsearch/showAward?AWD_ID=1550551&amp;HistoricalAwards=false">1550551</a>/<a ' \
 'href="https://nsf.gov/awardsearch/showAward?AWD_ID=1550461&amp;HistoricalAwards=false">1550461</a>/<a ' \
 'href="https://nsf.gov/awardsearch/showAward?AWD_ID=1550436&amp;HistoricalAwards=false">1550436</a>/<a ' \
 'href="https://nsf.gov/awardsearch/showAward?AWD_ID=1550514&amp;HistoricalAwards=false">1550514</a>, ' \
 'Any opinions, findings, and conclusions or ' \
 'recommendations expressed in this material are ' \
 'those of the author(s) and do not necessarily ' \
 'reflect the views of the National Science ' \
 'Foundation.</p>'

#===========================================

def relkey(name):
    g = re.match(r'^(.+)\s+(\S+)$',name)
    if name in release_team:
        idx = release_team.index(name)
        if idx == 0:
            return "A"+g.group(2)+", "+g.group(1)
        else:
            return "B"+g.group(2)+", "+g.group(1)
    else:
        return "Z"+g.group(2)+", "+g.group(1)

for name in release_team:
    assert name in names, name

names = sorted(names,key=relkey)

items = []
for name in names:
    cols = creators[name]
    affiliation = cols[2]
    item = {
        'name':re.sub(r'~',' ',name),
        'affiliation':cols[2]
    }
    if 3 < len(cols) and cols[3].strip() != '':
        orcid = cols[3].strip()
        assert re.match(r'^\d{4}(-[\dX]{4}){3}$',orcid), orcid
        item['orcid'] = orcid
    items += [item]

#c = {'metadata':{'creators':[]}}

c['metadata']['creators'] = items
c['metadata']['version'] = 'The "{et_release_codename}" release, {et_release}'.format(et_release=et_release, et_release_codename=et_release_codename)
c['metadata']['publication_date'] = datetime.datetime.now().strftime("%Y-%m-%d")
c['metadata']['description'] = et_description

if generate:
    if os.path.exists("Definition.md"):
        print("Not creating Definition.md. File already exists.")
    else:
        print("Creating Definition.md")
        with open("Definition.md","w") as fd:
            print(definition_md.strip(), file=fd)
    
    if os.path.exists("zupload.py"):
        print("Not creating zupload.py. File already exists.")
    else:
        print("Creating zupload.py")
        with open("zupload.py","w") as fd:
            pp = pprint.PrettyPrinter(stream=fd)
            pp.pprint(c)

if upload:
    with open("zupload.py","r") as fd:
        jdata = eval(fd.read())

    #ddiff(jdata, c)

    dep = requests.put("https://{server}/api/deposit/depositions/{id}".format(server=server,id=id),
        data=json.dumps(c),
        headers={"Content-Type": "application/json"},
        params={"access_token":access_token})
    if not dep.ok:
      print("request faild: %s\n%s" % (dep.status_code, dep.json()))
      dep.raise_for_status()

if publish:
    answer = input('Really publish (cannot be reverted)? Have you double checked zupload.py and have someone look at a dummy commit in the Zenodo sandbox? (yes/no) ')
    if answer != "yes":
        print("Aborting publishing")
        sys.exit(0)
    dep = requests.post("https://{server}/api/deposit/depositions/{id}/actions/publish".format(server=server,id=id),
        params={"access_token":access_token})
    if not dep.ok:
      print("request faild: %s\n%s" % (dep.status_code, dep.json()))
      dep.raise_for_status()
